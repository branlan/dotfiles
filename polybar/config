;=====================================================

[colours]

; dark
background = black
background-alt = #1B1D1E
foreground = #00ff00
primary = #bf5f4d
; end

; light
;background = ${xrdb:color7}
;background-alt = #cccccc
;foreground = ${xrdb:color0}
;primary = ${xrdb:color1}
; end


secondary = #bf5f4d
;alert = ${xrdb:purple}
alert = #d90404

;=====================================================

[sizes]
bar-width = 100%
bar-height = 50

bar-offset-x = 40
bar-top-gap = 20
bar-bottom-gap = 20

;=====================================================

[bar/example]
monitor = eDP-1
bottom = false
fixed-center = true
width = ${sizes.bar-width}
height = ${sizes.bar-height}
radius = 2.0

background = ${colours.background}
foreground = ${colours.foreground}

line-size = 2
line-color = #ffffff

border-size = 0
border-color = #00000000
border-left-size = ${sizes.bar-offset-x}
;border-top-size = ${sizes.bar-top-gap}
border-bottom-size = ${sizes.bar-bottom-gap}

padding-left = 0
padding-right = 5

module-margin-left = 2
module-margin-right = 3

font-0 = FontAwesome:size=20
font-1 = monospace:size=20
font-2 = monospace:size=20
font-3 = monospace:size=20
font-4 = monospace:size=20

modules-left = i3 cpu memory
modules-center = xwindow
modules-right = volume backlight-acpi battery

separator = |

tray-position = right
tray-detached = false
tray-padding = 1

scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

label-layout-underline = ${colours.primary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colours.primary}
label-indicator-underline = ${colours.primary}

;=====================================================

[global/wm]
margin-top = 5
margin-bottom = 5

;=====================================================

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

;=====================================================
;
;   Modules
;
;=====================================================

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

;=====================================================

[module/xbacklight]
type = internal/xbacklight

format = <ramp> <label>
label = : %percentage%%

ramp-0 = 
ramp-1 = 
ramp-2 = 

;=====================================================

[module/battery]
type = internal/battery
full-at = 100
battery = BAT0
adapter = ADP0

interval = 1

format-charging = <label-charging>
format-charging-foreground = ${colours.primary}
format-charging-underline = ${colours.primary}

format-discharging = <ramp-capacity> <label-discharging>
;format-discharging-underline = ${colours.primary}

format-full = <label-full>
format-full-foreground = ${colours.alert}
format-full-underline = ${colours.alert}

label-charging = : %percentage%%

label-discharging = : %percentage%%

label-full = : fully charged

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colours.foreground}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colours.primary}
animation-charging-framerate = 1000

;=====================================================

[module/cpu]
type = internal/cpu
interval = 1

format = <label>

label = CPU %percentage%%

ramp-load-0 = ▁
ramp-load-1 = ▂
ramp-load-2 = ▃
ramp-load-3 = ▄
ramp-load-4 = ▅
ramp-load-5 = ▆
ramp-load-6 = ▇
ramp-load-7 = █


;=====================================================

[module/date]
type = internal/date
interval = 1

label = %date%, %time%

date = %a %e %b
time = %I:%M %p
date-alt = %d.%m.%Y
time-alt = %H:%M

format =   <label>
format-foreground = ${colours.foreground}

;=====================================================

[module/filesystem]
type = internal/fs
mount-0 = /
interval = 30

format-mounted = <label-mounted>
label-mounted =   %mountpoint%: %free%

format-unmounted = <label-unmounted>
label-unmounted =   %mountpoint%: not mounted

;=====================================================

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
;wrapping-scroll = true

pin-workspaces = true
enable-click = true

label-mode = %mode%
label-mode-padding = 2
label-mode-background = ${colours.background}

label-focused = %name%
label-focused-foreground = green
label-focused-background = black
label-focused-underline = green
label-focused-padding = 4

label-unfocused = %name%
label-unfocused-padding = 4

label-visible = %name%
label-visible-underline = ${colours.secondary}
label-visible-padding = 4

label-urgent = %name%
;label-urgent-background = ${colours.alert}
label-urgent-underline = ${colours.alert}
label-urgent-padding = 4

;=====================================================

[module/memory]
type = internal/memory
interval = 2

format = <label>
label = RAM %percentage_used%%

;=====================================================

[module/mpd]
type = internal/mpd
format-playing =  <label-song> <icon-prev> <toggle> <icon-next>
format-paused =  <label-song> <icon-prev> <toggle> <icon-next>
format-stopped = 

label-song = %artist% - %title%
label-song-maxlen = 25
label-song-ellipsis = true

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

bar-progress-width = 20
bar-progress-indicator = |
bar-progress-fill = ─
bar-progress-empty = ─

;=====================================================

[module/wlan]
type = internal/network
interface = wlp58s0
interval = 3.0

format-connected = <label-connected>
;label-connected =  %ifname%: %local_ip% on %essid%, %signal%%
label-connected =  wifi: %essid%

format-disconnected = <label-disconnected>
label-disconnected =  wifi: dc
;label-disconnected =  dc


;==========================

[module/eth]
type = internal/network
interface = enp0s20f0u2
interval = 3.0

format-connected = <label-connected>
;label-connected =  %ifname%: %local_ip% on %essid%, %signal%%
label-connected =  eth: %essid%

format-disconnected = <label-disconnected>
label-disconnected =  eth: dc
;label-disconnected =  eth: dc

;=====================================================

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <label>
format-warn = <label-warn>
format-warn-underline = ${colours.alert}

label = %temperature%
label-warn = TEMP: %temperature%

;=====================================================

[module/volume]
type = internal/volume

format-volume = <label-volume>
label-volume = : %percentage%%

format-muted = <label-muted>
label-muted = : mute

;=====================================================

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format = <label-indicator>

label-indicator-padding = 3
label-indicator-background = ${colours.primary}
label-indicator-foreground = ${colours.background}

;=====================================================

[module/xwindow]
type = internal/xwindow
label = %title%
label-maxlen = 60

;=====================================================
;
;   End Modules
;
;=====================================================
